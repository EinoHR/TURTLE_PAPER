var amount = 4;
var turnRandomness = 2;

window.onload = function() {
    // Get a reference to the canvas object
    var canvas = document.getElementById('canvas');
    // Create an empty project and a view for the canvas:
    paper.setup(canvas);

    var turtles = [];

    for (let i = 1; i<amount+1; i++) {
        if (i == 1) {
            var turtle = new Turtle(
                paper.view.viewSize.width/2,
                paper.view.viewSize.height,
                {
                    strokeColor: 'black', 
                    strokeWidth: 4,
                }
            );
            turtle.turn(-90);
        } else if (i == 2) {
            var turtle = new Turtle(
                paper.view.viewSize.width,
                paper.view.viewSize.height/2,
                {
                    strokeColor: 'black', 
                    strokeWidth: 4,
                }
            );
            turtle.turn(180);
        } else if (i == 3) {
            var turtle = new Turtle(
                paper.view.viewSize.width/2,
                0,
                {
                    strokeColor: 'black', 
                    strokeWidth: 4,
                }
            );
            turtle.turn(90);
        } else {
            var turtle = new Turtle(
                0,
                paper.view.viewSize.height/2,
                {
                    strokeColor: 'black', 
                    strokeWidth: 4,
                }
            );
        }

        turtles.push(turtle);
    }

    paper.view.onFrame = function(event) {
        turtles.forEach(function(turtle) {
            turtle.turn(getRandomInt(-turnRandomness,turnRandomness));

            var newPoint = turtle.forward(3);

            for (let i = 0; i<turtles.length;i++) {
                let testTurtle = turtles[i];

                if (testTurtle != turtle) {
                    let nearestPoint = testTurtle.path.getNearestPoint(newPoint);

                    if (nearestPoint) {
                        if (getDistanceBetweenPoints(newPoint, nearestPoint) < 2) { 
                            turtle.turn(getRandomInt(-90,90));
                        }
                    }
                }
            }

            if (newPoint.x > paper.view.viewSize.width || newPoint.x < 0) {
                turtle.turn(90);
            }
            if (newPoint.y > paper.view.viewSize.height || newPoint.y < 0) {
                turtle.turn(90);
            }

            if (event.count % 100 == 0) {
                turtle.path.simplify(1);
            }
        });
    }


    paper.view.draw();
}


function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1) + min);
  }
  
function getDistanceBetweenPoints(point1, point2) {
    return Math.hypot(point1.x-point2.x, point1.y-point2.y);
}