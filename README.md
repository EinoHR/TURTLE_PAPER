# _TURTLE\_PAPER_

A small javascript "library" which adds [turtle graphics](https://en.wikipedia.org/wiki/Turtle_graphics)-style drawing to [Paper.js](https://paperjs.org/), a javascript canvas wrapper / drawing library with svg export, mouse interaction and tons of other features. 

## [View the website](https://xyny.codeberg.page/TURTLE_PAPER/)

## [Explore the examples](https://xyny.codeberg.page/TURTLE_PAPER/examples/)

## [Read the docs](https://xyny.codeberg.page/TURTLE_PAPER/docs/)

The code for TURTLE_PAPER is licensed under [GNU GPL v3](https://codeberg.org/xyny/TURTLE_PAPER/src/branch/pages/LICENSE).  
The written and artistic work for TURTLE_PAPER is licensed under [CC by-sa 4.0](https://creativecommons.org/licenses/by-sa/4.0/)