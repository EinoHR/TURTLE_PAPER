var turnRandomness = 45;

window.onload = function() {
    // Get a reference to the canvas object
    var canvas = document.getElementById('canvas');
    // Create an empty project and a view for the canvas:
    paper.setup(canvas);

    var randNum = getRandomInt(0,1);

    if (randNum == 0) {
        var turtle = new Turtle(
            paper.view.viewSize.width/2,
            0,
            {
                strokeColor: 'white', 
                strokeWidth: 4,
            }
        );
        turtle.turn(90);

        paper.view.onFrame = function(event) {
            if (event.count % 10 == 0) {
                turtle.turn(getRandomInt(-turnRandomness,turnRandomness));
    
                var newPoint = turtle.forward(20);
        
                if (newPoint.x > paper.view.viewSize.width) {
                    turtle.turn(90);
                } else if (newPoint.x < 0) {
                    turtle.turn(90);
                } else if (newPoint.y > paper.view.viewSize.height) {
                    turtle.turn(90);
                } else if (newPoint.y < 0) {
                    turtle.turn(90);
                }
            }
        }
    } 
    else if (randNum == 1) {
        var turtle = new Turtle(
            paper.view.viewSize.width/2,
            paper.view.viewSize.height/2,
            {
                strokeColor: 'white', 
                strokeWidth: 4,
            }
        );
        turtle.turn(90);

        paper.view.onFrame = function(event) {
            if (event.count % 10 == 0) {
                for (let i = 0; i<3; i++) {
                    let newPoint = turtle.forward(10);
                    turtle.turn(30);
        
                    if (newPoint.x > paper.view.viewSize.width || newPoint.x < 0 || newPoint.y > paper.view.viewSize.height || newPoint.y < 0) {
                        turtle.path.add(paper.view.viewSize/2);
                    }
                }
                let newPoint = turtle.forward(getRandomInt(-100,100));
                if (newPoint.x > paper.view.viewSize.width || newPoint.x < 0 || newPoint.y > paper.view.viewSize.height || newPoint.y < 0) {
                    turtle.path.add(paper.view.viewSize/2);
                }
            }
        }
    }


    paper.view.draw();
}


function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1) + min);
  }